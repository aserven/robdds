#ifndef _ROBDD_H_
#define _ROBDD_H_

#include <vector>
#include <unordered_map>

#include <sparsepp/spp.h>

using spp::sparse_hash_map;

#include <tuple>
namespace std{
    namespace
    {

        // Code from boost
        // Reciprocal of the golden ratio helps spread entropy
        //     and handles duplicates.
        // See Mike Seymour in magic-numbers-in-boosthash-combine:
        //     https://stackoverflow.com/questions/4948780

        template <class T>
        inline void hash_combine(std::size_t& seed, T const& v)
        {
            seed ^= hash<T>()(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
        }

        // Recursive template code derived from Matthieu M.
        template <class Tuple, size_t Index = std::tuple_size<Tuple>::value - 1>
        struct HashValueImpl
        {
          static void apply(size_t& seed, Tuple const& tuple)
          {
            HashValueImpl<Tuple, Index-1>::apply(seed, tuple);
            spp::hash_combine(seed, get<Index>(tuple));
          }
        };

        template <class Tuple>
        struct HashValueImpl<Tuple,0>
        {
          static void apply(size_t& seed, Tuple const& tuple)
          {
              spp::hash_combine(seed, get<0>(tuple));
          }
        };
    }


    template <typename ... TT>
    struct hash<std::tuple<TT...>> 
    {
        size_t
        operator()(std::tuple<TT...> const& tt) const
        {                                              
            size_t seed = 0;                             
            HashValueImpl<std::tuple<TT...> >::apply(seed, tt);    
            return seed;                                 
        }                                              

    };
}

class ROBDD {
public:

    struct bdd {
        int label;

        bdd* high;
        bdd* low;
    };

    ROBDD(const std::vector<int>& ordering);
    ~ROBDD();

    bdd* one();
    bdd* zero();

    bdd* var(int);
    bdd* nott(bdd* f);
    bdd* andd(bdd* f, bdd* g);
    bdd* orr(bdd* f, bdd* g);

private:

    bdd* ite(bdd* f, bdd* g, bdd* h);
    bool is_terminal_case(bdd* f, bdd* g, bdd* h);
    bdd* terminal_case_result(bdd* f, bdd* g, bdd* h);
    int get_top_variable(bdd* f, bdd* g, bdd* h);
    bdd* find_or_add_unique_table(int v, bdd* t, bdd* e);
    bdd* positive_cofactor(int v, bdd* f);
    bdd* negative_cofactor(int v, bdd* f);

    template<typename T1, typename T2, typename T3>
    struct triple {
        T1 first;
        T2 second;
        T3 third;

        triple(T1 x, T2 y, T3 z): first(x), second(y), third(z) {};

        bool operator==(const triple &t) const {
            return (first == t.first && second == t.second && third == t.third);
        }
    };

    // TODO
    // look for this, improve
    // or create hash before hashing
    // the int is created with the triple
    // map<int, bdd*>
    struct key_hash {
        template <class T1, class T2, class T3>
        std::size_t operator() (const triple<T1, T2, T3> &t) const {
            std::size_t h1 = std::hash<T1>()(t.first);
            std::size_t h2 = std::hash<T2>()(t.second);
            std::size_t h3 = std::hash<T3>()(t.third);

            return h1 ^ h2 ^ h3;
        }
    };

    // typedef triple<int, bdd*, bdd*> key_node;
    // typedef triple<bdd*, bdd*, bdd*> key_computation;
    typedef std::tuple<int, bdd*, bdd*> key_node;
    typedef std::tuple<bdd*, bdd*, bdd*> key_computation;

    // typedef std::unordered_map<key_node, bdd*, key_hash> map_nodes;
    // typedef std::unordered_map<key_computation, bdd*, key_hash> map_computations;
    // typedef std::unordered_map<key_node, bdd*> map_nodes;
    // typedef std::unordered_map<key_computation, bdd*> map_computations;

    typedef sparse_hash_map<key_node, bdd*> map_nodes;
    typedef sparse_hash_map<key_computation, bdd*> map_computations;
 
    bdd* _one;
    bdd* _zero;

    std::vector<int> var_ordering;
    std::vector<int> reverse_var_ordering;

    map_nodes unique_table;
    map_computations computed_table;

};

#endif
