# Reduced Ordered Binary Decision Diagram

Implementation of ROBDDs.

Used [sparsepp](https://github.com/greg7mdp/sparsepp) for hashing. 

## References

 - Sheldon B. Akers. Binary Decision Diagrams.
 - Karl S. Brace, Richard L. Rudell and Randal E. Bryant. Efficient Implementation of a BDD Package.
 - Randal E. Bryant. Graph-Based Algorithms for Boolean Function Manipulation


