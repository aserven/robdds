#include "ROBDD.h"

#include <vector>
#include <unordered_map>

#include <fstream>
#include <string>
#include <iostream>

using namespace std;

#define ONE  -1
#define ZERO -2


ROBDD::ROBDD(const vector<int>& ordering): var_ordering(ordering) {
    reverse_var_ordering = vector<int> (ordering.size());
    for(int i = 0; i < var_ordering.size(); i += 1) {
        reverse_var_ordering[var_ordering[i] - 1] =  i;
    }
    _one = new bdd{ONE, nullptr, nullptr};
    _zero = new bdd{ZERO, nullptr, nullptr};
}

ROBDD::~ROBDD() {
    delete _one;
    delete _zero;
    // cout << "Unique elements : " << unique_table.size() << endl;
    // cout << "Computed values : " << computed_table.size() << endl;

    for (auto it = unique_table.begin(); it != unique_table.end(); ++it) {
        delete it->second;
    }
    // cout << "Deleted ROBDD" << endl;
}


ROBDD::bdd* ROBDD::one() {
    return _one;
}

ROBDD::bdd* ROBDD::zero() {
    return _zero;
}

ROBDD::bdd* ROBDD::var(int variable) {
    key_node to_add = make_tuple(variable, _one, _zero);
    if (unique_table.find(to_add) != unique_table.end()) {
        return unique_table[to_add];
    } else {
        bdd* var_bdd = new bdd{variable, _one, _zero};
        unique_table[to_add] = var_bdd;
        return var_bdd;
    }
}

ROBDD::bdd* ROBDD::nott(bdd* f) {
    key_node to_add = make_tuple(f->label, f->low, f->high);
    if (unique_table.find(to_add) != unique_table.end()) {
        return unique_table[to_add];
    } else {
        bdd* not_bdd = new bdd{f->label, f->low, f->high};
        unique_table[to_add] = not_bdd;
        return not_bdd;
    }
}

ROBDD::bdd* ROBDD::andd(bdd* f, bdd* g) {
    return ite(f, g, _zero);
}

ROBDD::bdd* ROBDD::orr(bdd* f, bdd* g) {
    return ite(f, _one, g);
}

string print_label(int v) {
    if (v == ONE) {
        return "(1)";
    } else if (v == ZERO) {
        return "(0)";
    } else {
        return "x" + to_string(v);
    }
}


ROBDD::bdd* ROBDD::ite(bdd* f, bdd* g, bdd* h) {
    // cout << "  - ite " << 
        // print_label(f->label) << ", " << 
        // print_label(g->label) << ", " << 
        // print_label(h->label) << endl;
    if (is_terminal_case(f, g, h)) {
        // cout << "    Terminal case!" << endl;
        return terminal_case_result(f, g, h);
    }
    key_computation comp = make_tuple(f, g, h);
    if (computed_table.find(comp) != computed_table.end()) {
        // cout << "    COMPUTED" << endl;
        return computed_table[comp];
    } else {
        int v = get_top_variable(f, g, h);
        bdd* f_v = positive_cofactor(v, f);
        bdd* g_v = positive_cofactor(v, g);
        bdd* h_v = positive_cofactor(v, h);
        bdd* f_not_v = negative_cofactor(v, f);
        bdd* g_not_v = negative_cofactor(v, g);
        bdd* h_not_v = negative_cofactor(v, h);
        bdd* t = ite(f_v, g_v, h_v);
        bdd* e = ite(f_not_v, g_not_v, h_not_v);
        if (t == e) {
            return t;
        }
        // cout << " --- " << unique_table.size() << " ";
        bdd* r = find_or_add_unique_table(v, t, e);
        // cout << " --> " << unique_table.size() << endl;
        // cout << " xx> " << computed_table.size();
        computed_table[comp] = r;
        // cout << " xx> " << computed_table.size() << endl;
        return r;
    }
}

bool ROBDD::is_terminal_case(bdd* f, bdd* g, bdd* h) {
    return ((g == _one and h == _zero) or
            (f == _one) or
            (f == _zero) or
            (g == h) or
            (g == _zero and h == _one));
}

ROBDD::bdd* ROBDD::terminal_case_result(bdd* f, bdd* g, bdd* h) {
    if (g == _one and h == _zero) {   // ite(F, 1, 0) = F
        return f;
    } else if (f == _one) {      // ite(1, F, G) = F
        return g;
    } else if (f == _zero) {     // ite(0, G, F) = F
        return h;
    } else if (g == h) {        // ite(G, F ,F) = F
        return g;
    } else if (g == _zero and h == _one) { // ite(F, 0, 1) = not(F)
        return nott(f);
    } else {
        return f; // should not be reachable
    }
}

int ROBDD::get_top_variable(bdd* f, bdd* g, bdd* h) {
    int v = f->label;
    if (g->label > 0 and 
            reverse_var_ordering[v - 1] > reverse_var_ordering[g->label - 1]) {
        v = g->label;
    } else if (h->label > 0 and 
            reverse_var_ordering[v - 1] > reverse_var_ordering[h->label - 1]) {
        v = h->label;
    }
    return v;
}

ROBDD::bdd* ROBDD::find_or_add_unique_table(int v, bdd* t, bdd* e) {
    // cout << "  ~~ add to table [" << v << "]<" << 
        // print_label(t->label) << "," << 
        // print_label(e->label) << ">";
    key_node to_search = make_tuple(v, t, e);
    if (unique_table.find(to_search) != unique_table.end()) {
        // cout << "  # already in unique!" << endl;
        return unique_table[to_search];
    } else {
        bdd* result = new bdd{v, t, e};
        unique_table[to_search] = result;
        return result;
    }
}

ROBDD::bdd* ROBDD::positive_cofactor(int v, bdd* f) {
    return (v == f->label? f->high: f); 
}

ROBDD::bdd* ROBDD::negative_cofactor(int v, bdd* f) {
    return (v == f->label? f->low: f); 
}

